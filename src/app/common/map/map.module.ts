import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MapComponent} from "./map.component";
import {AgmCoreModule} from "@agm/core";
import {MapService} from "./map.service";
import {CamelizePipe} from "ngx-pipes";

@NgModule({
  imports: [
    CommonModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBKPeM4nuNQ_nyayFubzuLX2qVLVxPPOAc'
    })
  ],
  declarations: [
    MapComponent,
  ],
  exports: [
    MapComponent,
  ],
  providers: [
    MapService,
    CamelizePipe,
  ]
})
export class MapModule { }
