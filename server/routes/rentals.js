const express = require('express');
const router = express.Router();
const UserCtrl = require('../controllers/users');
const Rentals = require('../models/rental');

router.get('/secret', UserCtrl.authMiddleware, function(req, res) {
  res.json({"secret": true});
});

router.get('', function(req, res) {
  Rentals.find({}, function(err, foundRentals) {
    res.json(foundRentals);
  });
});

router.get('/:id', function(req, res) {
  const rentalId = req.params.id;
  Rentals.findById(rentalId, function(err, foundRental) {
    if(err) {
      res.status(422).send({errors: [{title: 'Rentals Error!', detail: 'Could not find Rentals!'}]});
    }
    res.json(foundRental);
  });
});

module.exports = router;
